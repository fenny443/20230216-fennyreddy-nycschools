package com.example.nycschools.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bumptech.glide.load.engine.Resource
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.flow.Flow

@Entity
data class School(
    @PrimaryKey
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val school_name: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("state_code")
    val state_code: String,
    @SerializedName("phone_number")
    val phone_number: String
)

@Entity
data class SatScores(
    @PrimaryKey
    val dbn: String,
    val school_name: String,
    val num_of_sat_test_takers: String,
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String,
    val sat_writing_avg_score: String
)

sealed class NetworkResult<T> {
    data class Loading<T>(val isLoading: Boolean) : NetworkResult<T>()
    data class Success<T>(val data: T) : NetworkResult<T>()
    data class Failure<T>(val errorMessage: String) : NetworkResult<T>()
}

sealed interface UiState<out T> {

    data class Success<T>(val data: T) : UiState<T>

    data class Error(val message: String) : UiState<Nothing>

    object Loading : UiState<Nothing>

}

