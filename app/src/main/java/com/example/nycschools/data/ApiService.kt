package com.example.nycschools.data

import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<School>>


    @GET("resource/f9bf-2cp4.json")
    suspend fun getSatScores(): Response<List<SatScores>>

}