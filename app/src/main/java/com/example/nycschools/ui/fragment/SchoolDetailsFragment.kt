package com.example.nycschools.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.nycschools.R
import com.example.nycschools.data.NetworkResult
import com.example.nycschools.data.SatScores
import com.example.nycschools.data.UiState
import com.example.nycschools.databinding.LayoutSatScoresBinding
import com.example.nycschools.ui.viewmodel.SchoolDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay

@AndroidEntryPoint
class SchoolDetailsFragment : Fragment() {

    private val viewModel: SchoolDetailsViewModel by viewModels()
    private lateinit var binding: LayoutSatScoresBinding
    private lateinit var satScores: List<SatScores>
    private lateinit var schoolName: String
    private val TAG = "SchoolDetailsFragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = LayoutSatScoresBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*
        * Read the data provided by the other fragment.
        */
        schoolName = arguments?.get("PAYLOAD").toString()
        println("** $TAG: onViewCreated() : ** $schoolName **")
        observeLiveData()
    }

    private fun observeLiveData() {
        viewModel.getUiState().observe(viewLifecycleOwner) {
            when (it) {
                is UiState.Loading -> {
                    println("**  $TAG: observeLiveData(): Loading **")
                    binding.scoresProgressbar.isVisible = true
                }
                is UiState.Error -> {
                    println("**  $TAG: observeLiveData(): Failure **")
                    binding.scoresProgressbar.isVisible = false
                }
                is UiState.Success -> {
                    satScores = it.data
                    println("** Success : observeLiveData(): scores list ize: ${satScores.size}**")
                    binding.scoresProgressbar.isVisible = false
                    fetchSchoolScores(schoolName, satScores)
                }
            }
        }
    }

    @VisibleForTesting
    fun fetchSchoolScores(schoolName: String, scoresList: List<SatScores>) {
        for (score in scoresList) {
            if (score.dbn.equals(schoolName, true)) {
                /*
                * Display the content layout if a match is found.
                */
                binding.scoresDataLayout.visibility = View.VISIBLE
                binding.noDataTV.visibility = View.GONE
                bindSchoolData(score)
                break
            } else {
                /*
                * show the error message as no match is found.
                */
                binding.scoresDataLayout.visibility = View.GONE
                binding.noDataTV.visibility = View.VISIBLE
            }
        }
    }

    private fun bindSchoolData(satScore: SatScores) {

        println("** $TAG:bindSchoolData : filtered school is: ${satScore.school_name} ")
        binding.schoolName.text = "${satScore.school_name} "
        binding.satTakers.text =
            "${context?.resources?.getString(R.string.sat_takers)} ${satScore.num_of_sat_test_takers}"
        binding.satReadingScore.text =
            "${context?.resources?.getString(R.string.reading)} ${satScore.sat_critical_reading_avg_score}"
        binding.satMathAvgScore.text =
            "${context?.resources?.getString(R.string.math)} ${satScore.sat_math_avg_score}"
        binding.satWritingAvgScore.text =
            "${context?.resources?.getString(R.string.writing)} ${satScore.sat_writing_avg_score}"
    }

}