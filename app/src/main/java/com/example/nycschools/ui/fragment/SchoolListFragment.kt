package com.example.nycschools.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.nycschools.R
import com.example.nycschools.data.NetworkResult
import com.example.nycschools.data.School
import com.example.nycschools.data.UiState
import com.example.nycschools.databinding.LayoutSchoolsListBinding
import com.example.nycschools.ui.viewmodel.SchoolsListViewModel
import com.example.nycschools.ui.adapter.ClickInterface
import com.example.nycschools.ui.adapter.SchoolListAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SchoolListFragment : Fragment() {

    private val viewModel: SchoolsListViewModel by viewModels()
    private val TAG = "SchoolListFragment"

    @Inject
    lateinit var movieAdapter: SchoolListAdapter

    private lateinit var binding: LayoutSchoolsListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutSchoolsListBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        movieAdapter.setItemClick(object : ClickInterface<School> {
            override fun onClick(data: School) {
                println("** $TAG: setItemClick() : ${data.dbn}  **")
                /**
                 * I have used Navigation graph for the navigation. I have already defined the action
                 * to navigate to SchoolDetailsFragment in nav graph.
                 *  Please refer to the navigation/main.xml inorder to get an idea on the navigation
                 *  in the project.
                 * I am also attaching the "School name" to the bundle inorder to detect the
                 * selected School.
                 */
                findNavController().navigate(
                    R.id.action_school_list_to_school_details,
                    bundleOf("PAYLOAD" to data.dbn)
                )
            }
        })

        binding.retryButton.setOnClickListener { observeLiveData() }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.schoolsRecyclerView.adapter = null
    }


    private fun observeLiveData() {
        viewModel.getUiState().observe(viewLifecycleOwner) {
            when (it) {
                is UiState.Loading -> {
                    binding.progressbar.isVisible = true
                    println("** TAG: observeLiveData(): Loading **")
                }
                is UiState.Error -> {
                    println("** TAG: observeLiveData(): FAILURE **")
                    binding.progressbar.isVisible = false
                    binding.errorLayout.visibility = View.VISIBLE
                    binding.schoolsRecyclerView.visibility = View.GONE
                }
                is UiState.Success -> {
                    binding.errorLayout.visibility = View.GONE
                    binding.schoolsRecyclerView.visibility = View.VISIBLE
                    movieAdapter.updateSchools(it.data)
                    binding.schoolsRecyclerView.adapter = movieAdapter
                    binding.progressbar.isVisible = false
                }
            }
        }
    }


}