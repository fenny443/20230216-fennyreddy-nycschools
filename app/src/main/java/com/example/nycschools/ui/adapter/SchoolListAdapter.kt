package com.example.nycschools.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.data.School
import com.example.nycschools.databinding.SchoolDetailsBinding
import javax.inject.Inject

/**
 *  This adapter is used to render the schools content to the UI.
 *  The click event is set to each item in the onBindViewHolder().
 **/

class SchoolListAdapter @Inject constructor() :
    RecyclerView.Adapter<SchoolListAdapter.MovieViewHolder>() {

    var schools = mutableListOf<School>()
    private var clickInterface: ClickInterface<School>? = null


    fun updateSchools(schools: List<School>) {
        this.schools = schools.toMutableList()
        /*
        * Update the UI for any change in the information.
        */
        notifyItemRangeInserted(0, schools.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding =
            SchoolDetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {

        val item = schools[position]
        println("** $position : ${item.school_name}")
        holder.view.sName.text = item.school_name
        holder.view.city.text = item.city
        holder.view.state.text = item.state_code
        holder.view.phone.text = item.phone_number
        /*
        * set the click event for each item.
        * This event is detected by the Fragment & handles the UI redirection.
        */
        holder.view.movieCard.setOnClickListener {
            clickInterface?.onClick(item)
        }
    }

    override fun getItemCount(): Int {
        return schools.size
    }

    fun setItemClick(clickInterface: ClickInterface<School>) {
        this.clickInterface = clickInterface
    }

    class MovieViewHolder(val view: SchoolDetailsBinding) : RecyclerView.ViewHolder(view.root)
}

interface ClickInterface<T> {
    fun onClick(data: T)
}