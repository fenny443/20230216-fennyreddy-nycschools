package com.example.nycschools.ui.repository

import com.example.nycschools.data.*
import javax.inject.Inject

/*
* This repository holds the logic to make the API calls
*/
class SchoolRepository @Inject constructor(private val apiService: ApiService) {

    suspend fun getSchools(): UiState<List<School>> {
        val response = apiService.getSchools()
        return if (response.isSuccessful) {
            val responseBody = response.body()
            if (responseBody != null) {
                UiState.Success(responseBody)
            } else {
                UiState.Error(response.toString())
            }
        } else {
            UiState.Error(response.toString())
        }
    }

    suspend fun getSatScores(): UiState<List<SatScores>> {
        val response = apiService.getSatScores()
        return if (response.isSuccessful) {
            val responseBody = response.body()
            if (responseBody != null) {
                UiState.Success(responseBody)
            } else {
                UiState.Error(response.toString())
            }
        } else {
            UiState.Error(response.toString())
        }
    }


}