package com.example.nycschools.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.data.NetworkResult
import com.example.nycschools.data.SatScores
import com.example.nycschools.data.School
import com.example.nycschools.data.UiState
import com.example.nycschools.ui.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(
    private val schoolRepository: SchoolRepository
) : ViewModel() {

    private val uiState = MutableLiveData<UiState<List<SatScores>>>()

    private val errorMessage = MutableLiveData<String>()

    var job: Job? = null
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }

    init {
        fetchSatScores()
    }

    private fun fetchSatScores() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            uiState.postValue(UiState.Loading)
            val response = schoolRepository.getSatScores()
            withContext(Dispatchers.Main) {

                when (response) {
                    is UiState.Success -> {
                        uiState.postValue(response)
                    }
                    is UiState.Error -> {
                        uiState.postValue(response)
                    }
                    else -> {
                        uiState.postValue(UiState.Loading)
                    }
                }
            }
        }

    }

    private fun onError(message: String) {
        errorMessage.value = message
        uiState.postValue(UiState.Error(errorMessage.toString()))
    }

    fun getUiState(): LiveData<UiState<List<SatScores>>> {
        return uiState
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}

