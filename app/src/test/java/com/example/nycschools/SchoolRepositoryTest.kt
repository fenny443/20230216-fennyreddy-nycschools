package com.example.nycschools

import com.example.nycschools.data.ApiService
import com.example.nycschools.data.SatScores
import com.example.nycschools.data.School
import com.example.nycschools.di.NetworkModule
import com.example.nycschools.ui.fragment.SchoolDetailsFragment
import com.example.nycschools.ui.repository.SchoolRepository
import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import java.net.HttpURLConnection
import javax.inject.Inject


@OptIn(ExperimentalCoroutinesApi::class)
class SchoolRepositoryTest {

    private lateinit var repository: SchoolRepository
    private lateinit var apiService: ApiService
    private lateinit var mockWebServer: MockWebServer
    private lateinit var retrofitInstance: Retrofit

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        retrofitInstance = NetworkModule.provideRetrofit(NetworkModule.provideOkHttp())
        apiService = NetworkModule.provideApiClient(retrofitInstance)
        repository = SchoolRepository(apiService)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `for no schools, api must return empty with http code 200`(): Unit = runTest {
        val users = emptyList<School>()
        val expectedResponse = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Gson().toJson(users))
        mockWebServer.enqueue(expectedResponse)

        GlobalScope.launch {
            val actualResponse = repository.getSchools()
            assertThat((actualResponse as List<*>).isEmpty())
        }

    }


    @Test
    fun `for multiple users, api must return all users with http code 200`() = runTest {
        val schools = listOf(
            School("02M260","Clinton School Writers & Artists, M.S. 260","Manhattan","NY", phone_number = "6758880920"),
            School("08X282","Women's Academy of Excellence","Bronx","NY", phone_number = "9998880000"),
            School("17K548","Brooklyn School for Music & Theatre","Brooklyn","NY", phone_number = "8877440000")
        )
        val expectedResponse = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Gson().toJson(schools))
        mockWebServer.enqueue(expectedResponse)

        GlobalScope.launch {
            val actualResponse = repository.getSchools()
            assertThat((actualResponse as List<*>).size == 3)
            assertThat(actualResponse == schools)
        }

    }


}