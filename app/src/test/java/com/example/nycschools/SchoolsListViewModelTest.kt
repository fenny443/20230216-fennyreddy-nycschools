package com.example.nycschools

import com.example.nycschools.data.ApiService
import com.example.nycschools.data.School
import com.example.nycschools.data.UiState
import com.example.nycschools.ui.repository.SchoolRepository
import com.example.nycschools.ui.viewmodel.SchoolsListViewModel
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import retrofit2.Response

@ExperimentalCoroutinesApi
class SchoolsListViewModelTest {


    private val testDispatcher = TestCoroutineDispatcher()

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MaincoroutineRule()

    // Subject under test
    private lateinit var viewModel: SchoolsListViewModel
    private lateinit var repository: SchoolRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        repository = mock(SchoolRepository::class.java)
        viewModel = SchoolsListViewModel(repository)
    }

    @Test
    fun getSchoolsTest() {
        runBlocking {
            Mockito.`when`(repository.getSchools())
                .thenReturn(UiState.Success(listOf(School("movie", "", "new", "", ""))))
            viewModel.fetchSchools()
            val result = viewModel.getUiState()
            assertEquals(listOf(School("movie", "", "new", "", "")), result)
        }
    }

    @Test
    fun `empty school list test`() {
        runBlocking {
            Mockito.`when`(repository.getSchools())
                .thenReturn(UiState.Success(listOf()))
            viewModel.fetchSchools()
            val result = viewModel.getUiState()
            assertEquals(listOf<School>(), result)
        }
    }

}








