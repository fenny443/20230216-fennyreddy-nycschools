package com.example.nycschools

import com.example.nycschools.di.NetworkModule
import org.junit.Test
import retrofit2.Retrofit

class RetrofitClientTest {
    @Test
    fun testRetrofitInstance() {
        //Get an instance of Retrofit
        val instance: Retrofit = NetworkModule.provideRetrofit(NetworkModule.provideOkHttp())
        //Assert that, Retrofit's base url matches to our BASE_URL
        assert(instance.baseUrl().toUrl().toString() == NetworkModule.provideBaseURl())
    }
}